CRC Config
=========

This role configures a locally running CodeReady Containers instance to
authenticate using a set of pre-defined users, with optional cluster-admin
privileges.

This role can also set a custom logo for the web console, just for that extra
personal touch.

Requirements
------------

In order to set user passwords this role uses the Ansible `htpasswd` module,
this requires the python `passlib` module to be installed.

This role also makes extensive use of the `k8s` module and related plugins.
This requires the python `PyYAML` and `openshift` modules.

Role Variables
--------------

The following variables are used:

- `crc_config_users`
   Required.
   A list of users, for example:
   ```yaml
   crc_config_users:
   - username: alice
     fullname: Alice
     is_admin: True
     password: redhat
   - username: bob
     fullname: Bob
     is_admin: false
     password: redhat
   - username: mary
     password: redhat
   ```
   The `fullname` and `is_admin` attributes are optional, and will default to
   None and False respectively.  If any user has `is_admin` set to `true` a
   (new) group `crc-cluster-admins` will be created will all those users as
   members, and a new ClusterRoleBinding is created to give that group
   cluster-admin privileges.
- `crc_config_users_delete_others`
  Optional, default: False
  Whether to remove any existing users from the cluster (True) or not (False).
- `crc_config_users_delete_kubeadmin`
  Optional, default: False
  Whether to remove the default `kube:admin` user or not.
  This role will refuse to delete `kube:admin` if no users have been marked as
  `is_admin: true`
- `crc_config_custom_logo_file`
  Optional, default: None
  A path to an iamge to set as custom logo for the web console.
- `crc_config_custom_product_name`
  Optional, default: None
  A custom product name to set in the web console.

Dependencies
------------

N/A

Example Playbook
----------------

```yaml
---
- name: Configure a Code Ready Container cluster
  hosts:
  - localhost
  become: false
  gather_facts: no
  roles:
  - role: crc-config
    vars:
      crc_config_users:
      - username: alice
        fullname: Alice
        is_admin: True
        password: redhat
      - username: bob
        fullname: Bob
        is_admin: false
        password: redhat
      - username: mary
        password: redhat
      crc_config_users_delete_others: true
      crc_config_users_delete_kubeadmin: true
      crc_config_custom_logo_file: /home/example/Pictures/custom_logo.svg
```


License
-------

BSD

